<?php
include_once "config.php";
include_once "funcoes.php";
$tabela = "avaliacao";

for ($j=0; $j < $reps; $j++) {
  $result = $PDO->query("truncate $tabela");
  $inicio_tempo = time();
  $inicio_dados = qtd_dados();
  for ($i=0; $i < $linhas; $i++) {
    $link_id_aluno = rand(111111111,999999999999);
    $data_avaliacao = date('Y-m-d G:i:s');
    $link_id_usuario = rand(111111111,999999999999);
    $texto_avaliacao = base64_encode(md5(rand(1,99999999))).base64_encode(md5(rand(1,99999999)));
    if($cripto) $texto_avaliacao = $Cripto_OO->encrypt($texto_avaliacao);
    $anexos = base64_encode(md5(rand(1,99999999))).base64_encode(md5(rand(1,99999999)));
    if($cripto) $anexos = $Cripto_OO->encrypt($anexos);
    $data_criacao = date('Y-m-d G:i:s');
    $data_ultima_alteracao = date('Y-m-d G:i:s');
    $privacidade = "Externa";
    $profissional =  base64_encode(md5(rand(1,99999999)));
    if($cripto) $profissional = $Cripto_OO->encrypt($profissional);
    $profissao =  base64_encode(md5(rand(1,99999999)));
    $link_id_nucleo = rand(111111111,999999999999);
    $sql = "insert into $tabela
    ('link_id_aluno','data_avaliacao','link_id_usuario','texto_avaliacao','anexos','data_criacao',
    'data_ultima_alteracao','privacidade','profissional','profissao','link_id_nucleo')
    values
    ('$link_id_aluno','$data_avaliacao','$link_id_usuario','$texto_avaliacao','$anexos','$data_criacao',
    '$data_ultima_alteracao','$privacidade','$profissional','$profissao','$link_id_nucleo')
    ";
    $result = $PDO->query( $sql );
  }
  $fim_tempo =  time();
  $fim_dados =  qtd_dados();
  $tempos[] = $fim_tempo - $inicio_tempo ;
  $dados[] = $fim_dados - $inicio_dados ;
}
echo "<br>-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*INICIO DADOS $tabela*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
<br>
Dados insert_$tabela.php
Linhas: $linhas <br>
Repetições: $reps \n <br> Cripto: $cripto <br>
<br>";
estatisticas_tempo($tempos);
echo "<br>";
estatisticas_dados($dados);
echo "<br>
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*FIM DADOS $tabela*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-";
unset($tempos);
unset($dados);
