-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 25-Maio-2018 às 11:04
-- Versão do servidor: 5.6.40
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sia`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `aluno`
--

CREATE TABLE `aluno` (
  `id` int(11) NOT NULL,
  `documento` mediumtext COLLATE latin1_general_ci,
  `link_id_escola` int(11) DEFAULT NULL,
  `link_id_nucleo` int(11) NOT NULL,
  `obs` text COLLATE latin1_general_ci,
  `data_criacao` datetime DEFAULT NULL,
  `data_ultima_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nome` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `data_nascimento` date DEFAULT NULL,
  `cod_controle` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `arquivos_aluno` mediumtext COLLATE latin1_general_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacao`
--

CREATE TABLE `avaliacao` (
  `id` int(11) NOT NULL,
  `link_id_aluno` int(11) NOT NULL,
  `data_avaliacao` date NOT NULL,
  `link_id_usuario` int(11) DEFAULT NULL,
  `texto_avaliacao` text CHARACTER SET latin1 NOT NULL,
  `anexos` text COLLATE latin1_general_ci,
  `data_criacao` datetime DEFAULT NULL,
  `data_ultima_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `privacidade` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `profissional` mediumtext COLLATE latin1_general_ci,
  `profissao` mediumtext COLLATE latin1_general_ci,
  `link_id_nucleo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `escola`
--

CREATE TABLE `escola` (
  `id` int(11) NOT NULL,
  `documento` text COLLATE latin1_general_ci,
  `nome` text CHARACTER SET latin1 NOT NULL,
  `email` text CHARACTER SET latin1,
  `data_criacao` datetime DEFAULT NULL,
  `data_ultima_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `senha` mediumtext COLLATE latin1_general_ci NOT NULL,
  `nome_usuario` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `arquivos` mediumtext COLLATE latin1_general_ci,
  `link_id_nucleo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `nucleo`
--

CREATE TABLE `nucleo` (
  `id` int(11) NOT NULL,
  `documento` text COLLATE latin1_general_ci,
  `nome` text COLLATE latin1_general_ci NOT NULL,
  `email` text CHARACTER SET latin1,
  `obs` text COLLATE latin1_general_ci,
  `data_criacao` datetime DEFAULT NULL,
  `data_ultima_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `arquivos` mediumtext COLLATE latin1_general_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `documento` text CHARACTER SET latin1 NOT NULL,
  `nome` text CHARACTER SET latin1 NOT NULL,
  `email` text CHARACTER SET latin1 NOT NULL,
  `senha` text CHARACTER SET latin1,
  `admin_nucleo` text COLLATE latin1_general_ci,
  `link_id_nucleo` int(11) DEFAULT NULL,
  `link_id_escola` int(11) DEFAULT NULL,
  `data_criacao` datetime DEFAULT NULL,
  `data_ultima_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nome_usuario` varchar(50) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aluno`
--
ALTER TABLE `aluno`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `avaliacao`
--
ALTER TABLE `avaliacao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `escola`
--
ALTER TABLE `escola`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nucleo`
--
ALTER TABLE `nucleo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aluno`
--
ALTER TABLE `aluno`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4972;

--
-- AUTO_INCREMENT for table `avaliacao`
--
ALTER TABLE `avaliacao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `escola`
--
ALTER TABLE `escola`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nucleo`
--
ALTER TABLE `nucleo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
