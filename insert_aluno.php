<?php
include_once "config.php";
include_once "funcoes.php";
$tabela = "aluno";

for ($j=0; $j < $reps; $j++) {
  $result = $PDO->query("truncate $tabela");
  $inicio_tempo = time();
  $inicio_dados = qtd_dados();
  for ($i=0; $i < $linhas; $i++) {
    $documento = rand(111111111,999999999999);
    $link_id_escola = rand(1111,9999);
    $link_id_nucleo = rand(1111,9999);
    $obs = 'obs';
    $data_criacao = date('Y-m-d G:i:s');
    $data_ultima_alteracao = date('Y-m-d G:i:s');
    $nome = base64_encode(md5(rand(1,9999))); //Gera strings aleatorias
    if($cripto) $nome = $Cripto_OO->encrypt($nome);
    $data_nascimento = date('Y-m-d G:i:s');
    $cod_controle = rand(1111,9999);
    $arquivos_aluno = base64_encode(md5(rand(1,9999))).base64_encode(md5(rand(1,9999)));
    if($cripto) $arquivos_aluno = $Cripto_OO->encrypt($arquivos_aluno);
    $sql = "insert into $tabela
    (documento,link_id_escola,link_id_nucleo,obs,data_criacao,
    data_ultima_alteracao,nome,data_nascimento,cod_controle,arquivos_aluno)
    values
    ('$documento','$link_id_escola','$link_id_nucleo','$obs','$data_criacao',
    '$data_ultima_alteracao','$nome','$data_nascimento','$cod_controle','$arquivos_aluno')
    ";
    $result = $PDO->query( $sql );
  }
  $fim_tempo =  time();
  $fim_dados =  qtd_dados();
  $tempos[] = $fim_tempo - $inicio_tempo ;
  $dados[] = $fim_dados - $inicio_dados ;
}
echo "<br>-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*INICIO DADOS $tabela*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
<br>
Dados insert_$tabela.php
Linhas: $linhas <br>
Repetições: $reps \n <br> Cripto: $cripto <br>
<br>";
estatisticas_tempo($tempos);
echo "<br>";
estatisticas_dados($dados);
echo "<br>
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*FIM DADOS $tabela*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-";
unset($tempos);
unset($dados);
