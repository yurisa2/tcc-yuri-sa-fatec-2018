<?php
include_once "config.php";
include_once "funcoes.php";
$tabela = "nucleo";

for ($j=0; $j < $reps; $j++) {
  $result = $PDO->query("truncate $tabela");
  $inicio_tempo = time();
  $inicio_dados = qtd_dados();
  for ($i=0; $i < $linhas; $i++) {
    $documento = rand(111111111,999999999999);
    $nome = base64_encode(md5(rand(1,99999999))); //Gera strings aleatorias
    if($cripto) $nome = $Cripto_OO->encrypt($nome);
    $email = base64_encode(md5(rand(1,99999999)));
    if($cripto) $email = $Cripto_OO->encrypt($email);
    $obs = 'obs';
    $data_criacao = date('Y-m-d G:i:s');
    $data_ultima_alteracao = date('Y-m-d G:i:s');
    $arquivos_nucleo = base64_encode(md5(rand(1,99999999))).base64_encode(md5(rand(1,99999999)));
    if($cripto) $arquivos_nucleo = $Cripto_OO->encrypt($arquivos_nucleo);
    $sql = "insert into $tabela
    ('documento','nome','email','obs','data_criacao','
    data_ultima_alteracao','arquivos_nucleo')
    values
    ('$documento','$nome','$email','$obs','$data_criacao','
    $data_ultima_alteracao','$arquivos_nucleo')
    ";
    $result = $PDO->query( $sql );
  }
  $fim_tempo =  time();
  $fim_dados =  qtd_dados();
  $tempos[] = $fim_tempo - $inicio_tempo ;
  $dados[] = $fim_dados - $inicio_dados ;
}
echo "<br>-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*INICIO DADOS $tabela*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
<br>
Dados insert_$tabela.php
Linhas: $linhas <br>
Repetições: $reps \n <br> Cripto: $cripto <br>
<br>";
estatisticas_tempo($tempos);
echo "<br>";
estatisticas_dados($dados);
echo "<br>
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*FIM DADOS $tabela*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-";
unset($tempos);
unset($dados);
