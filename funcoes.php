<?php
error_reporting(E_ALL & ~E_NOTICE);
$tempos = array();
$dados = array();

$PDO = new PDO('mysql:host=localhost;dbname=sia', $user, $pass);


echo '<pre>';

class Encryption //100% StackOverflow
{
  const CIPHER = MCRYPT_RIJNDAEL_128; // Rijndael-128 is AES
  const MODE   = MCRYPT_MODE_CBC;

  /* Cryptographic key of length 16, 24 or 32. NOT a password! */
  private $key;
  public function __construct($key) {
    $this->key = $key;
  }

  public function encrypt($plaintext) {
    $ivSize = mcrypt_get_iv_size(self::CIPHER, self::MODE);
    $iv = mcrypt_create_iv($ivSize, MCRYPT_DEV_URANDOM);
    $ciphertext = mcrypt_encrypt(self::CIPHER, $this->key, $plaintext, self::MODE, $iv);
    return base64_encode($iv.$ciphertext);
  }

  public function decrypt($ciphertext) {
    $ciphertext = base64_decode($ciphertext);
    $ivSize = mcrypt_get_iv_size(self::CIPHER, self::MODE);
    if (strlen($ciphertext) < $ivSize) {
      throw new Exception('Missing initialization vector');
    }

    $iv = substr($ciphertext, 0, $ivSize);
    $ciphertext = substr($ciphertext, $ivSize);
    $plaintext = mcrypt_decrypt(self::CIPHER, $this->key, $ciphertext, self::MODE, $iv);
    return rtrim($plaintext, "\0");
  }
}

$Cripto_OO = new Encryption("FATEC20181234567");

function Stand_Deviation($arr)
{
  $num_of_elements = count($arr);

  $variance = 0.0;

  // calculating mean using array_sum() method
  $average = array_sum($arr)/$num_of_elements;

  foreach($arr as $i)
  {
    // sum of squares of differences between
    // all numbers and means.
    $variance += pow(($i - $average), 2);
  }

  return (float)sqrt($variance/$num_of_elements);
}

function estatisticas_tempo($tempos) {
  // var_dump($tempos);
  echo "Tempo Mínimo: ".min($tempos);
  echo "<br>";
  echo "Tempo Médio: ".(array_sum($tempos)/count($tempos));
  echo "<br>";
  echo "Tempo Máximo: ".max($tempos);
  echo "<br>";
  echo "Desvio Padrão do tempo de cada passe: ".Stand_Deviation($tempos);
}

function estatisticas_dados($dados) {
  // var_dump($dados);
  echo "Bytes Mínimo: ".min($dados);
  echo "<br>";
  echo "Bytes Médio: ".(array_sum($dados)/count($dados));
  echo "<br>";
  echo "Bytes Máximo: ".max($dados);
  echo "<br>";
  echo "Desvio Padrão dos bytes de cada passe: ".Stand_Deviation($dados);
}

function qtd_dados()
{
  global $PDO;
  $stmt = $PDO->query("SELECT * FROM information_schema.session_status
    WHERE variable_name IN ('Bytes_received');");
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    return $row[VARIABLE_VALUE];
  }


  ?>
