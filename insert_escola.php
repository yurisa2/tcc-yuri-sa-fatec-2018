<?php
include_once "config.php";
include_once "funcoes.php";
$tabela = "escola";

for ($j=0; $j < $reps; $j++) {
  $result = $PDO->query("truncate $tabela");
  $inicio_tempo = time();
  $inicio_dados = qtd_dados();
  for ($i=0; $i < $linhas; $i++) {
    $documento = rand(111111111,999999999999);
    $nome = base64_encode(md5(rand(1,99999999))); //Gera strings aleatorias
    if($cripto) $nome = $Cripto_OO->encrypt($nome);
    $email = base64_encode(md5(rand(1,99999999)));
    if($cripto) $email = $Cripto_OO->encrypt($email);
    $data_criacao = date('Y-m-d G:i:s');
    $data_ultima_alteracao = date('Y-m-d G:i:s');
    $senha = base64_encode(md5(rand(1,99999999)));
    if($cripto) $senha = $Cripto_OO->encrypt($senha);
    $nome_usuario = base64_encode(md5(rand(1,99999999))); //Gera strings aleatorias
    $arquivos = base64_encode(md5(rand(1,99999999))).base64_encode(md5(rand(1,99999999)));
    if($cripto) $arquivos = $Cripto_OO->encrypt($arquivos);
    $link_id_nucleo = rand(111111111,999999999999);
    $sql = "insert into $tabela
    ('documento','nome','email','data_criacao','
    data_ultima_alteracao','senha','nome_usuario','arquivos','link_id_nucleo')
    values
    ('$documento','$nome','$email','$data_criacao','$data_ultima_alteracao',
    '$senha','$nome_usuario','$arquivos','$link_id_nucleo')
    ";
    $result = $PDO->query( $sql );
  }
  $fim_tempo =  time();
  $fim_dados =  qtd_dados();
  $tempos[] = $fim_tempo - $inicio_tempo ;
  $dados[] = $fim_dados - $inicio_dados ;
}
echo "<br>-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*INICIO DADOS $tabela*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
<br>
Dados insert_$tabela.php
Linhas: $linhas <br>
Repetições: $reps \n <br> Cripto: $cripto <br>
<br>";
estatisticas_tempo($tempos);
echo "<br>";
estatisticas_dados($dados);
echo "<br>
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*FIM DADOS $tabela*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-";
unset($tempos);
unset($dados);
