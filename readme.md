YURI VASCONCELOS DE ALMEIDA SÁ

2650831523026

Fatec São Roque


### A CRIPTOGRAFIA DE DADOS SENSÍVEIS EM SISTEMAS DE INTEGRAÇÃO SOCIAL OPEN-SOURCE


###### Trabalho de Graduação apresentado à Faculdade de Tecnologia São Roque, como parte dos requisitos necessários para a obtenção do título de Tecnólogo em Sistemas para Internet.


###### Orientador: Prof.ª Mª Adriana Paula Borges

São Roque

2018


## 

~-*~-*~-*~-*~-*~-*~-*~-*~-*~-*~-*~-*~-*~-*~-*~-*~-*~-*~-*~-*~-*~-*~-*~-*~-*~-*



Neste repositório encontram-se os scripts de apoio 
para utilização e recriação dos dados contidos no trabalho:




config.php - Configurações, incluido em todos os outros scripts

funcoes.php - Funcoes de criptografia, controle e aquisição de dados

sia.sql - Arquivo para criação do banco de dados 

insert_$nome_da_tabela - Arquivos para o insert seriado da tabela

gera.php - Arquivo para a geração das estatísticas do trabalho
